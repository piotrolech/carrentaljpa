package com.capgemini.starterkit.service;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
 
import com.capgemini.starterkit.dto.CarDto;
import com.capgemini.starterkit.dto.CreateUpdateCarDto;
import com.capgemini.starterkit.entity.Car;
import com.capgemini.starterkit.entity.CarType;
import com.capgemini.starterkit.service.CarService;
 
@RunWith(SpringRunner.class)
@SpringBootTest
public class CarServiceTest {
   
    @Autowired
    CarService carService;
   
    @Test
    public void testShouldFindCarById() {
        // given
        final int carId = 2;
        // when
        Car car = carService.findById(carId);
        // then
        assertNotNull(car);
        assertEquals("crossover", car.getCategory());
    }
   
    @Test
    public void testShouldAddCar() {
        // given
        int countBefore = carService.findAllCars().size();
        CarDto carDto = new CarDto();
        carDto.setCategory("Test category");
        carDto.setColor("Test color");
        carDto.setHorsepower(120);
        carDto.setProductionYear(2017);
        carDto.setMileage(new BigDecimal(100000));
        carDto.setCapacity(new BigDecimal(1.7));
        // when
        CarDto savedCar = carService.addCar(carDto);
        // then
        assertNotNull(savedCar.getId());
        assertEquals(countBefore+1, carService.findAllCars().size());
    }
    
    @Test
    public void testShouldDeleteCar() {
        // given
        int countBefore = carService.findAllCars().size();
        // when
        carService.deleteCar(1);
        // then
        assertEquals(countBefore-1, carService.findAllCars().size());
    }
 
}