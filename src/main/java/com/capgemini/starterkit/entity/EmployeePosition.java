package com.capgemini.starterkit.entity;

public enum EmployeePosition {
    SALESMAN, DIRECTOR, ACCOUNTANT
}
