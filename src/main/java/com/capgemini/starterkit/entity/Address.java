package com.capgemini.starterkit.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "addresses")
public class Address extends AbstractEntity {


    @Size(max = 20)
    @NotNull
    private String country;
    @Size(max = 20)
    private String district;
    @Size(max = 30)
    @NotNull
    private String city;
    @Size(max = 30)
    private String street;
    @Size(max = 10)
    @Column(name = "house_no")
    private String houseNumber;
    @Column(name = "flat_no")
    private Integer flatNumber;


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Integer getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(Integer flatNumber) {
        this.flatNumber = flatNumber;
    }
}
