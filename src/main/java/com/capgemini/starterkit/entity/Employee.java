package com.capgemini.starterkit.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.util.Set;

@Entity
@Table(name = "employees")
public class Employee extends AbstractEntity {


    @ManyToOne
    @JoinColumn(name = "branche_id")
    private Branch branch;

    @Size(max = 30)
    @NotNull
    @Column(name = "first_name")
    private String firstName;

    @Size(max = 50)
    @NotNull
    @Column(name = "last_name")
    private String lastName;

    @Embedded
    private EmployeePosition position;

    @NotNull
    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @ManyToMany(mappedBy = "carWatchers", fetch = FetchType.LAZY)
    private Set<Car> watchedCars;

    public Set<Car> getWatchedCars() {
        return watchedCars;
    }

    public void setWatchedCars(Set<Car> watchedCars) {
        this.watchedCars = watchedCars;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public EmployeePosition getPosition() {
        return position;
    }

    public void setPosition(EmployeePosition position) {
        this.position = position;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "branch=" + branch +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", position=" + position +
                ", dateOfBirth=" + dateOfBirth +
                ", watchedCars:" + watchedCars.size() +
                '}';
    }
}
