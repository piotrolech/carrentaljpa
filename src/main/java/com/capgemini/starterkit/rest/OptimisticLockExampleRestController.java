package com.capgemini.starterkit.rest;

import com.capgemini.starterkit.dto.CarDto;
import com.capgemini.starterkit.entity.Car;
import com.capgemini.starterkit.repository.CarRepository;
import com.capgemini.starterkit.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@RestController
public class OptimisticLockExampleRestController {

	@Autowired
	private CarRepository carRepository;

	@RequestMapping("/optimistic-lock")
	public void optimisticLock() {


		Car car1 = carRepository.findById(1);
		Car car2 = carRepository.findById(2);

		car2.setColor("abc");
		carRepository.save(car2);

		System.out.println("Zapisano zmieny w obiekcie, wersja obiektu: " + car2.getVersion());

		System.out.println("Próbuję zapisać obiekt, wersja obiektu: " + car1.getVersion());
		car1.setColor("abc");
		carRepository.save(car1);

	}

}
