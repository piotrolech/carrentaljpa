package com.capgemini.starterkit.repository;

import com.capgemini.starterkit.entity.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeesRepository  extends CrudRepository<Employee, Integer>{
}
