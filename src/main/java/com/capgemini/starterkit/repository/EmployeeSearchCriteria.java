package com.capgemini.starterkit.repository;

import com.capgemini.starterkit.entity.EmployeePosition;

public class EmployeeSearchCriteria {
    private Integer branchId;
    private Integer carId;
    private EmployeePosition employeePosition;

    public EmployeeSearchCriteria() {
    }

    public EmployeeSearchCriteria(Integer branchId, Integer carId, EmployeePosition employeePosition) {

        this.branchId = branchId;
        this.carId = carId;
        this.employeePosition = employeePosition;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public EmployeePosition getEmployeePosition() {
        return employeePosition;
    }

    public void setEmployeePosition(EmployeePosition employeePosition) {
        this.employeePosition = employeePosition;
    }
}
